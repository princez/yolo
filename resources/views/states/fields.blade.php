<div class="row">
    {!! csrf_field() !!}
    
    <div class="col-md-3 form-group">
        <label>Active</label>
        <select class="form-control" name="is_active">
        @foreach(['No','Yes'] as $g=>$t)
        <option {{old('is_active',$State->is_active??null)==$g?'selected':''}} value="{{$g}}">{{$t}}</option>
        @endforeach
        </select>
        <p class="error">
            {{$errors->first('is_active')}}
        </p>
    </div>
 
    <div class="col-md-6 form-group">
        <label>State</label>
        <input type="text" name="title" placeholder="Enter State Name Here"  class="form-control" value="{{old('title',$State->title??'')}}" />
        <p class="error">
            {{$errors->first('title')}}
        </p>
    </div>

   
    
</div>
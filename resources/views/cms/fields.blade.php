<div class="row">
    {!! csrf_field() !!}
    <div class="col-md-12  form-group">
        <label>Name</label>
        <input type="text" name="title"  class="form-control" value="{{old('title',$Cms->title??'')}}" />
        <p class="error">
            {{$errors->first('title')}}
        </p>
    </div>

   
    <div class="col-md-12 form-group">
        <label>Content<small></small></label>
        <textarea name="content" rows="10"  id="t_content" class="form-control">{{old('content',$Cms->content??'')}}</textarea>
        <p class="error">
            {{$errors->first('content')}}
        </p>
    </div>
    
</div>

@push('scripts')
<script src="{{url('assets\vendor\ckeditor5\ckeditor.js')}}"></script>
<script>
    const READY = ()=>{
        CKEDITOR.replace('t_content');
    }
</script>
@endpush
<div class="row">
    {!! csrf_field() !!}
  
    <div class="col-md-12  col-sm-12 form-group">
        <label>Organization</label>
        <input type="text" name="name"  class="form-control" value="{{old('name',$Data->name??'')}}" />
        <p class="error">
            {{$errors->first('name')}}
        </p>
    </div>
    <div class="col-md-6  col-sm-6 form-group">
        <label>Registration Number</label>
        <input type="text" name="registration_number"  class="form-control" value="{{old('registration_number',$Data->registration_number??'')}}" />
        <p class="error">
            {{$errors->first('registration_number')}}
        </p>
    </div>

    <div class="col-md-6 col-sm-6  form-group">
        <label>Logo</label>
        <input type="file" name="logo" class="form-control" />
        <p class="error">
            {{$errors->first('logo')}}
        </p>
    </div>
    

    <div class="col-md-12">
        <h4>Address</h4>
        <hr>
    </div>

    <div class="col-md-4 col-sm-4 form-group">
        <label>State</label>
        <select  class="form-control " previousValue="{{old('state_id',$Data->state_id??null)}}" target="[name='city_id']" name="state_id">
            @foreach($states as $state)
            <option {{old('state_id',$Data->state_id??null)==$state->id?'selected':''}} value="{{$state->id}}">{{$state->title}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('state_id')}}
        </p>
    </div>
    <div class="col-md-4 col-sm-4 form-group">
        <label for="">City</label>
        <select url="{{route('ajax.getCities')}}"  previousValue="{{old('city_id',$Data->city_id??null)}}"  name="city_id" id="" class="form-control">

        </select>
        <p class="error">
            {{$errors->first('city_id')}}
        </p>
    </div>

    <div class="col-md-4  col-sm-4 form-group">
        <label>Zipcode</label>
        <input type="text" name="zipcode"  class="form-control" value="{{old('zipcode',$Data->zipcode??'')}}" />
        <p class="error">
            {{$errors->first('zipcode')}}
        </p>
    </div>

    <div class="col-md-12 col-sm-12 form-group">
        <label for="">Address</label>
        <textarea name="address" id="" class="form-control" >{{old('address',$Data->address??null)}}</textarea>
        <p class="error">
            {{$errors->first('address')}}
        </p>
    </div>

    <div class="col-md-12">
        <h4>Bank Account Details</h4>
        <hr>
    </div>

    
</div>
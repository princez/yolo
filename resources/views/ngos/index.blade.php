@extends('layouts.default')
@section('title','Non Profit Organizations')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">NGOs</h6> <a href="{{route('ngos.create')}}" class="btn btn-default float-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create NGO</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
          <table class="table table-bordered table-striped d_table">
            <thead>
              <tr>
                <th>#</th>
                <th>Org.</th>
                <th>Reg. No.</th>
                <th>City</th>
                <th>Logo</th>
                <th>Updated At</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $d)
                <tr>
                  <td>
                    {{$d->id}}
                  </td>
                  <td>
                    {{$d->name}}
                  </td>
                  <td>
                    {{$d->registration_number}}
                  </td>
                  <td>
                    {{$d->city}}
                  </td>
                  <td>
                      <img src="{{$d->logo}}" style="max-height:10vh;" class="rounded" alt="">
                  </td>
                  <td>
                    {!! getDateColumn($d) !!}
                  </td>
                  <td>
                    <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="{{ route('ngos.edit', $d->id) }}" class='btn btn-link'>
                      <i class="fa fa-edit"></i>
                    </a>
                    <a onclick="trashUtil(this)" trash-url="{{ route('ngos.destroy', $d->id) }}" trash-item-to-remove="tr" data-toggle="tooltip" data-placement="bottom" title="Delete" href="javascript:void(0)" class='btn btn-link'>
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush
@extends('layouts.default')
@section('title','Edit Campaign Type')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">Edit Campaign Type</h6> <a href="{{route('types.index')}}" class="btn btn-default float-right"><i class="fa fa-arrow-left"></i>&nbsp;Back to Campaign Type</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
            <form method="post" action="{{route('types.update',$Data->id)}}" enctype="multipart/form-data">
              @method('PUT')  
              @include('campaign-types.fields')
                <div class="col-md-12 form-group">
                  <button type="submit" class="btn btn-success float-right">Update</button>
                </div>
            </form>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

@endpush
@php $ROUTE = Route::currentRouteName(); 

@endphp
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
  <title>{{env('APP_NAME')}}-@yield('title')</title>
  
  @include('layouts.partials.links')
  <style>
    .badge-info{
      background-color: #0eccef!important;
    }
  </style>
</head>

<body>
  <!-- Sidenav -->
  @include('layouts.partials.sidenav')
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
   @include('layouts.partials.topnav')
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
        @yield('header')
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      @yield('content')
     
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
                &copy; 2020 <a href="#" class="font-weight-bold ml-1" target="_blank">{{env('APP_NAME')}}</a>
            </div>
          </div>
          <div class="col-lg-6">
           
          </div>
        </div>
      </footer>
    </div>
  </div>
 @include('layouts.partials.scripts')
</body>

</html>
 
@php 
  $routes =[
    [
      'title'=>"Dashboard",
      'icon'=>"ni ni-tv-2 text-primary",
      'route'=>'home',
      'match'=>'home'
   ],
   [
      'title'=>"Campaign Types",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'types.index',
      'match'=>'types.index'
   ],

   [
      'title'=>"NGOs",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'ngos.index',
      'match'=>'ngos.index'
   ],

   [
      'title'=>"Campaigns",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'campaigns.index',
      'match'=>'campaigns.index'
   ],

   [
      'title'=>"States",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'states.index',
      'match'=>'states.index'
   ],

   [
      'title'=>"Cities",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'cities.index',
      'match'=>'cities.index'
   ],
  
   [
      'title'=>"CMS",
      'icon'=>"ni ni-ui-04 text-danger",
      'route'=>'cms.index',
      'match'=>'cms '
   ]

  ];
@endphp
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header bg-primary  align-items-center">
        <a class="" href="javascript:void(0)">
          {{-- <h2 style="color:#5e72e4; font-weight:1000;">{{env('APP_NAME')}}</h2> --}}
        <img style="height:12vh;" src="{{url('images/logo.png')}}" alt="">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            @foreach($routes as $route)
              <li class="nav-item">
              <a class="nav-link {{(Request::is($route['match'].'*') || $ROUTE==$route['match'] )?'active':''}}" href="{{route($route['route'])}}">
                  <i class="{{$route['icon']}}"></i>
                  <span class="nav-link-text">{{$route['title']}}</span>
                </a>
              </li>
            @endforeach
            
           
            <li class="nav-item">
             <a class="nav-link" onclick="logout()" href="javascript:void(0)">
                <i class="ni ni-user-run text-dark"></i>
                <span class="nav-link-text">Logout</span>
              </a>
            </li>
          </ul>
        
         
       
        </div>
      </div>
    </div>
  </nav>
<div class="row">
    {!! csrf_field() !!}
  
    <div class="col-md-6  col-sm-6 form-group">
        <label>Campaign Name</label>
        <input type="text" name="name"  class="form-control" value="{{old('name',$Data->name??'')}}" />
        <p class="error">
            {{$errors->first('name')}}
        </p>
    </div>
    

    <div class="col-md-6 col-sm-6  form-group">
        <label>Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control" />
        <p class="error">
            {{$errors->first('thumbnail')}}
        </p>
    </div>
    

   

    <div class="col-md-4 col-sm-4 form-group">
        <label>Campaign Type</label>
        <select  class="form-control "   name="campaign_type_id">
            <option value="">Select</option>
            @foreach($types as $type)
            <option {{old('campaign_type_id',$Data->campaign_type_id??null)==$type->id?'selected':''}} value="{{$type->id}}">{{$type->name}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('campaign_type_id')}}
        </p>
    </div>

    <div class="col-md-8 col-sm-8 form-group">
        <label>Non Profit Organization</label>
        <select  class="form-control "   name="ngo_id">
            <option value="">Select</option>

            @foreach($ngos as $type)
            <option {{old('ngo_id',$Data->ngo_id??null)==$type->id?'selected':''}} value="{{$type->id}}">{{$type->name}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('ngo_id')}}
        </p>
    </div>
    

    <div class="col-md-4  col-sm-4 form-group">
        <label>No. of Meals</label>
        <input type="number" min="1" name="no_of_meals"  class="form-control" value="{{old('no_of_meals',$Data->no_of_meals??'')}}" />
        <p class="error">
            {{$errors->first('no_of_meals')}}
        </p>
    </div>

    <div class="col-md-4  col-sm-4 form-group">
        <label>Cost Per Meal</label>
        <input type="text"  name="cost_per_meal"  class="form-control" value="{{old('cost_per_meal',$Data->cost_per_meal??'')}}" />
        <p class="error">
            {{$errors->first('cost_per_meal')}}
        </p>
    </div>

    <div class="col-md-4  col-sm-4 form-group">
        <label>End Date</label>
        <input type="text"  name="end_date" autocomplete="off"  class="form-control _datepicker" value="{{old('end_date',$Data->end_date??'')}}" />
        <p class="error">
            {{$errors->first('end_date')}}
        </p>
    </div>

    <div class="col-md-12 col-sm-12 form-group">
        <label for="">Description</label>
        <textarea name="description" id="" class="form-control" >{{old('description',$Data->description??null)}}</textarea>
        <p class="error">
            {{$errors->first('description')}}
        </p>
    </div>

    

    
</div>

@push('scripts')
<script>
    const READY = ()=>{
        $('._datepicker').datepicker({
		language: 'en',
		dateFormat:"yyyy-mm-dd",
        minDate:new Date()
	});
    }
</script>
@endpush
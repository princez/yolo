<div class='btn-group btn-group-sm'>
  <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="{{ route('states.edit', $id) }}" class='btn btn-link'>
    <i class="fa fa-edit"></i>
  </a>
  <a onclick="trashUtil(this)" trash-url="{{ route('states.destroy', $id) }}" trash-item-to-remove="tr" data-toggle="tooltip" data-placement="bottom" title="Delete" href="javascript:void(0)" class='btn btn-link'>
    <i class="fa fa-trash"></i>
  </a>
</div>

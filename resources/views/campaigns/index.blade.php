@extends('layouts.default')
@section('title','Campaigns')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">Campaigns</h6> <a href="{{route('campaigns.create')}}" class="btn btn-default float-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create Campaign</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
          <table class="table table-bordered table-striped d_table">
            <thead>
              <tr>
                <th>#</th>
                <th>Campaign</th>
                <th>Thumbnail</th>
                <th>Gallery</th>
                <th>Updated At</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $d)
                <tr>
                  <td>
                    {{$d->id}}
                  </td>
                  <td>
                    {{$d->name}}
                  </td>
                  <td>
                      <img src="{{$d->thumbnail}}" style="max-height:10vh;" class="rounded" alt="">
                      
                      
                  </td>
                  <td>
                    <a href="{{route('campaigns.gallery',$d->id)}}" class="btn btn-info btn-sm rounded">Gallery</a>
                  </td>
                  <td>
                    {!! getDateColumn($d) !!}
                  </td>
                  <td>
                    {{-- <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="{{ route('ngos.edit', $d->id) }}" class='btn btn-link'>
                      <i class="fa fa-edit"></i>
                    </a>
                    <a onclick="trashUtil(this)" trash-url="{{ route('ngos.destroy', $d->id) }}" trash-item-to-remove="tr" data-toggle="tooltip" data-placement="bottom" title="Delete" href="javascript:void(0)" class='btn btn-link'>
                      <i class="fa fa-trash"></i>
                    </a> --}}
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush
@extends('layouts.default')
@section('title','Gallery')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">{{$Campaign->name}}</h6> <a href="{{route('campaigns.index')}}" class="btn btn-default float-right"><i class="fa fa-arrow-left"></i>&nbsp;Back to Campaigns</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-header bg-success text-white text-center">
            Add Image to Gallery
        </div>
        <div class="card-body">
            <form action="" enctype="multipart/form-data" method="POST">
                <div class="row">
                    {!! csrf_field() !!}
                    <div class="col-md-4 col-sm-4 form-group">
                        <label for="">Image</label>
                        <input  type="file" multiple="multiple" name="image[]" class="form-control">
                        <p class="error">
                            {{$errors->first('image')}}
                        </p>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary rounded float-right">
                            Add Image
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>  
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
           <table class="table table-bordered table-striped">
               <thead>
                   <tr>
                       <th>#</th>
                       <th>Image</th>
                       <th>Actions</th>
                   </tr>
               </thead>
               <tbody>
                   @foreach($images as $i=>$image)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>
                           <img src="{{$image->image}}" style="max-height: 100px;" class="img img-responsive rounded" alt="">
                        </td>
                        <td>
                            <a onclick="trashUtil(this)" trash-url="{{ route('campaigns.gallery.destroy', $image->id) }}" trash-item-to-remove="tr" data-toggle="tooltip" data-placement="bottom" title="Delete" href="javascript:void(0)" class='btn btn-link'>
                                <i class="fa fa-trash"></i>
                              </a>
                        </td>
                    </tr>
                   @endforeach
               </tbody>
           </table>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush
@extends('mails.default')
@section('content')
<center><p style="font-size:20px; color:#2d4052;" align="center" ><b>Reset Password</b></p></center>
<center><p style="font-size:12px; color:#2d4052;" align="center" >A password reset event has been triggered. The password reset window is limited to two hours.</p></center>
<center><p style="font-size:12px; color:#2d4052;" align="center" >If you do not reset your password within two hours, you will need to submit a new request.</p></center>
<center><p style="font-size:12px; color:#2d4052;" align="center" >To complete the password reset process, visit the following link:
</p></center>
<center>
    <a href="{{route('resetPassword',$link->token)}}">{{route('resetPassword',$link->token)}}</a>
</center>

@endsection
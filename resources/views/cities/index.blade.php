@extends('layouts.default')
@section('title','Cities')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">Cities</h6> <a href="{{route('cities.create')}}" class="btn btn-default float-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create City</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
            {{$dataTable->table()}}
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')
{{$dataTable->scripts()}}
<script>
   
</script>
@endpush
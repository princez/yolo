<div class="row">
    {!! csrf_field() !!}
    
    <div class="col-md-3 form-group">
        <label>Active</label>
        <select class="form-control" name="is_active">
        @foreach(['No','Yes'] as $g=>$t)
        <option {{old('is_active',$City->is_active??null)==$g?'selected':''}} value="{{$g}}">{{$t}}</option>
        @endforeach
        </select>
        <p class="error">
            {{$errors->first('is_active')}}
        </p>
    </div>
    <div class="col-md-3 form-group">
        <label>State</label>
        <select class="form-control" name="state_id">
            @foreach($states as $state)
            <option {{old('state_id',$City->state_id??null)==$state->id?'selected':''}} value="{{$state->id}}">{{$state->title}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('state_id')}}
        </p>
    </div>
    
    <div class="col-md-6 form-group">
        <label>City</label>
        <input type="text" name="title" placeholder="City TItle goes here "  class="form-control" value="{{old('title',$City->title??'')}}" />
        <p class="error">
            {{$errors->first('title')}}
        </p>
    </div>

   
    
</div>
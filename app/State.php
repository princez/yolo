<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;


class State extends Model
{ 
    use SoftDeletes;    

    protected $fillable =[
        'is_active',
        'title',
        'country_id'
    ];
    protected static function booted()
    {
        static::addGlobalScope('only_active', function (Builder $builder) {
            $builder->where('states.is_active', 1);
        });
        // static::addGlobalScope('only_india', function (Builder $builder) {
        //     $builder->where('states.country_id', 101);
        // });
    }
}

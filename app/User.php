<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;



class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email', 'password','mobile',
        'is_approved',
        'is_banned',
        'gender_id',
        'profile',
        'role',
        'dob'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();  // Eloquent model method
    }

   
    public function getJWTCustomClaims()
    {
        return [
             'user' => [ 
                'id' => $this->id 
             ]
        ];
    }

    public function getProfileAttribute($v){
        return url($v);
    }

    public function gender(){
        return $this->hasOne('App\Gender','id','gender_id');
    }

    

    public function getProfile(){
         $user=User::with('gender')
                    ->select(
                            'dob',
                            'role',
                            'full_name',
                            'mobile',
                            'email',
                            'profile',
                            'gender_id'
        )
                    ->find($this->id);
        $temp = clone $user;            
        unset($temp->gender_id);
        if($temp->gender){
            unset($temp->gender->status);
            
        }
        return $temp;   
    }
}

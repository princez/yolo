<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ngo extends Model
{
    //
    use SoftDeletes;
    protected $fillable =[
        'name',
        'registration_number',
        'logo',
        'city_id',
        'zipcode',
        'address',
    ];

    public function getLogoAttribute($v){
        return url($v);
    }
}

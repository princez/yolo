<?php

namespace App\Helpers;
use \Session;

class Flash 
{
	//
	protected $errorMsg="An unknown error occured while processing your request !";

    public function __call($methodName,$args){
    	Session::flash('alert-message',$args[0]??$this->errorMsg);
    	Session::flash('alert-icon',$this->getIcon($methodName));
        if($args[1]??false)
            Session::flash('alert-toast',true);
        
    }

    public function getIcon($methodName){
    	$icon;
    	switch ($methodName) {
    		case 'info':
    			$icon="info";
    		break;
			case 'e':
			case 'err':	
			case 'error':	
                $icon="error";
            break;
    		default:
    			$icon="success";
    			break;
    	}

       return $icon; 
    }
}

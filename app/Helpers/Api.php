<?php
 
namespace App\Helpers;


class Api 
{
    //

    public function errorMsg(){
    	return "An unknown error occured while processing your request !";
    }

    public function notValid($data=[]){
        return response()->json(array_merge($data,['error'=>true]),422);
    }

    public function error($data=[]){
    	return response()->json(array_merge($data,['error'=>true,'errorMsg'=>$this->errorMsg()]),500);
    }

    public function success($data=[]){
    	return response()->json(array_merge($data,['error'=>false]),200);
    }

    public function notFound($data=[]){
        return response()->json(array_merge($data,['error'=>false]),404);   
    }

    public function custom($data,$statusCode=407){

        return response()->json(array_merge($data,['error'=>false]),$statusCode);
    }
}

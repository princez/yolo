<?php
if (!function_exists('flash')) {
    function flash()
    {
        return new \App\Helpers\Flash;
    }
}

if (!function_exists('otp')) {
    function otp()
    {
        return new \App\Otp;
    }
}

if (!function_exists('api')) {
    function api()
    {
        return new \App\Helpers\Api;
    }
}

if (!function_exists('upload')) {
    function upload($file,$directory="/")
    {   
        $directory='ups/'.$directory;
        $fileName = uniqid().'.'.$file->getClientOriginalExtension();
        $file->move($directory,$fileName);
        return $directory.$fileName;
    }
}

 
 function sendMail($email,$subject,$view,$data=[]){
    // return view($view);
    // dd($data);
    // try{
        return \Mail::send($view,$data, function ($message)use($subject,$email){
                        $message->from(env('MAIL_FROM_ADDRESS'),env('MAIL_USERNAME'));
                        $message->to($email);
                        $message->subject($subject);
      });
    // }    
    // catch(\Exception $e){
        // dd($e);
    // }
    
  } 

  function getCurrencyIndex(){
    return \Session::get('currency',0);
  }

  function getCurrency(){

     if(empty(currencies[getCurrencyIndex()]) )
        return currencies[0];

      return currencies[getCurrencyIndex()];  
  }

  function getExchangeRate(){
    $cr=  getCurrency()[0];
    $d=file_get_contents("https://api.exchangeratesapi.io/latest?base=USD&symbols=".$cr); 
    $d= json_decode($d);
    return round($d->rates->$cr,2);
  }

  function getRandomString($n) { 
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randomString = ''; 
  
    for ($i = 0; $i < $n; $i++) { 
        $index = rand(0, strlen($characters) - 1); 
        $randomString .= $characters[$index]; 
    } 
  
    return $randomString;  
} 


function getDateColumn($modelObject, $attributeName = 'updated_at')
{
    if (1) {
        $html = '<p data-toggle="tooltip" data-placement="bottom" title="${date}">${dateHuman}</p>';
    } else {
        $html = '<p data-toggle="tooltip" data-placement="bottom" title="${dateHuman}">${date}</p>';
    }
    if (!isset($modelObject[$attributeName])) {
        return '';
    }
    $dateObj = new Carbon\Carbon($modelObject[$attributeName]);
    $replace = preg_replace('/\$\{date\}/', $dateObj->format('l jS F Y (h:i:s)'), $html);
    $replace = preg_replace('/\$\{dateHuman\}/', $dateObj->diffForHumans(), $replace);
    return $replace;
}

function convert_seconds($seconds) 
 {
        $str="";
        $dt1 = new DateTime("@0");
        $dt2 = new DateTime("@$seconds");
        // $dt1->diff($dt2)->format('%a days, %h hours, %i minutes and %s seconds');
        $temp =$dt1->diff($dt2);
        if($temp->format("%a")!=0)
            $str.=$temp->format("%a")." days ";
        
        if($temp->format("%h")!=0)
            $str.=$temp->format("%h")." hours ";
        
        if($temp->format("%i")!=0)    
             $str.=$temp->format("%i")." minutes";
        return $str;
  }    
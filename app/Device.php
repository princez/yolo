<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    //
    public $timestamps = false;
    
    protected $fillable  = [
        'user_id',
        'fcm_token'
    ];

    
}

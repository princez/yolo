<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class City extends Model
{
    //
    protected $fillable = ['title','is_active','state_id'];
    protected static function booted()
    {
        static::addGlobalScope('only_active', function (Builder $builder) {
            $builder->where('cities.is_active', 1);
        });

        // static::addGlobalScope('only_india', function (Builder $builder) {
        //     $builder->join('states','states.id','=','cities.state_id')
        //             ->where('states.country_id', 101);
        // });
    }

    public function state(){
        return $this->hasOne('App\State','id','state_id');
    }
}

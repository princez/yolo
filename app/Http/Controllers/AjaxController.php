<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
class AjaxController extends Controller
{
    //
    public function uploadMedia(Request $req){
        $input['upload']=$req->file('upload');

        $rules = [
                        'upload'=>'required|image|max:8000'
        ];

        $validate = \Validator::make($input,$rules);
        if($validate->fails())
            return response()->json(['uploaded'=>0,'error'=>['message'=>$validate->errors()->first()]]);
            // return 2323;
        $res['fileName']=$req->file('upload')->getClientOriginalName();
       
         $res['image']=upload($req->file('upload'),'blogs/');
         $res['url']='/'.$res['image'];
         $res['uploaded']=1;
        $res['url'] = url($res['url']);
         return $res;
         
    }

    public function getCities($state_id){
        $cities = City::whereStateId($state_id)
                        ->select('cities.id','cities.title as name')
                        ->orderBy('cities.title')
                        ->get();

       return api()->success(['data'=>$cities]);                         
    }
}

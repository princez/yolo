<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\ReviewsDataTable;
use App\Review;


class ViewController extends Controller
{
    //
    public function home(Request $req){
        // return $req->all();
        return view('home');
    }

    public function categories(CategoriesDataTable $table){
        return $table->render('categories');
    }

    public function reviews(ReviewsDataTable $table){
        return $table->render('reviews.index');
    }

    public function reports(){
        return view('reports.index');
    }
}

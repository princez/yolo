<?php

namespace App\Http\Controllers;

use App\CampaignType;
use Illuminate\Http\Request;

use Illuminate\Support\Arr;
use \Validator;

class CampaignTypeController extends Controller
{
    protected $attributes = [
        'name',
        
    ];

    protected $validationRules =[
        'name'=>'required|min:2|max:200|unique:campaign_types,name,NULL,id,deleted_at,NULL',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = CampaignType::all();
        return view('campaign-types.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('campaign-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
      
        
       if(CampaignType::create($input)){
           flash()->ok('Campaign type created successfully !');
           return redirect()->route('types.index');
       }

       flash()->err();
       return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Data = CampaignType::findOrFail($id);
        return view('campaign-types.edit',compact('Data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $req)
    {
        //
        $Category = CampaignType::findOrFail($id);
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;
       
        $rules['name']="required|min:2|max:200|unique:campaign_types,name,$id,id,deleted_at,NULL";

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
       


       if($Category->update($input)){
           flash()->ok('Campaign Type Updated Successfully !');
           return redirect()->route('types.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        if(CampaignType::findOrFail($id)->delete())
            return api()->success(['message'=>"Campaign type deleted successfully !"]);

        return api()->error();
       
    }
}

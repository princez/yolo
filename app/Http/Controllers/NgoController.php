<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;

use App\DataTables\StatesDataTable;
use App\State;
use App\Ngo;
 
class NgoController extends Controller
{
    protected $attributes =[
        'name',
        'registration_number',
        'logo',
        'state_id',
        'city_id',
        'zipcode',
        'address',
        
        
    ];

    protected $validationRules = [
        'name'=>'required|min:2|max:200',
        'registration_number'=>'required|min:1|max:100',
        'logo'=>'required|image|max:2000',
        'state_id'=>'required',
        'city_id'=>'required',
        'zipcode'=>'required',
        'address'=>'required|max:200',                               
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Ngo::leftJoin('cities','cities.id','=','ngos.city_id')
                    ->select(
                        'cities.title as city',
                        'ngos.*'
                    )
                    ->orderBy('ngos.updated_at','DESC')
                    ->get();
        return view('ngos.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $states = State::orderBy('title','ASC')->select('id','title')->get();
        return view('ngos.create',compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Arr::only($request->all(),$this->attributes);
        $validate = Validator::make($input,$this->validationRules);
        if($validate->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validate);
        
        $input['logo']=upload($request->file('logo'),'ngos/');

        if(Ngo::create($input)){
            flash()->ok("Ngo added successfully !");
            return redirect()->route('ngos.index');
        }
        flash()->e();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Data = Ngo::findOrFail($id);
        $states = State::orderBy('title','ASC')->select('id','title')->get();
        return view('ngos.edit',compact('Data','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Ngo = Ngo::findOrFail($id);
        $input = Arr::only($request->all(),$this->attributes);
        $rules = $this->validationRules;
        $rules['logo']=str_replace('required','nullable',$rules['logo']);
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validate);
        
        if($request->hasFile('logo')){
            $input['logo']=upload($request->file('logo'),'ngos/');
        }else{
            unset($input['logo']);
        }
        if($Ngo->update($input)){
            flash()->ok("Ngo updated successfully !");
            return redirect()->route('ngos.index');
        }
        flash()->e();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Ngo::findOrFail($id)->delete())
            return api()->success(['message'=>"Action completed successfully !"]);

        return api()->error();
    }
}

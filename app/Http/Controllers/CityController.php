<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;

use App\DataTables\CitiesDataTable;
use App\State;
use App\City;

class CityController extends Controller
{

    protected $attributes =[
        'title',
        'state_id',
        'is_active'
    ];

    protected $validationRules = [
        'title'=>'required|min:2|regex:/^[\pL\s\-]+$/u|max:300|unique:cities,title,NULL,id,deleted_at,NUll',
         'is_active'=>'required|in:1,0',                                
        'state_id'=>"required|exists:states,id"
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CitiesDataTable $table)
    {
        //
        return $table->render('cities.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $states = State::all();
        return view('cities.create',compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = Arr::only($request->all(),$this->attributes);
        $validate = Validator::make($input,$this->validationRules);
        if($validate->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validate);
        
        if(City::create($input)){
            flash()->ok("City added successfully !");
            return redirect()->route('cities.index');
        }
        flash()->e();
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $City = City::withoutGlobalScope('only_active')->select('cities.*')->findOrFail($id);
        $states = State::all();
        return view('cities.edit',compact('City','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Sector = City::withoutGlobalScope('only_active')
                        
                                    ->select('cities.*')
                                    ->findOrFail($id);
        $rules = $this->validationRules;
        $rules['title']="required|min:2|regex:/^[\pL\s\-]+$/u|max:300|unique:cities,title,$id,id,deleted_at,NUll";
        $input = Arr::only($request->all(),$this->attributes);
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validate);
        
        if($Sector->update($input)){
            flash()->ok("City updated successfully !");
            return redirect()->route('cities.index');
        }
        flash()->e();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(City::findOrFail($id)->delete())
             return api()->success(['message'=>"Action completed successfully !"]);

        return api()->error();
    }
}

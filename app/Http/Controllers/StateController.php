<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;

use App\DataTables\StatesDataTable;
use App\State;
use App\City;
 
class StateController extends Controller
{
    protected $attributes =[
        'title',
        'is_active'
    ];

    protected $validationRules = [
        'title'=>'required|min:2|regex:/^[\pL\s\-]+$/u|max:300|unique:states,title,NULL,id,deleted_at,NUll',
        'is_active'=>'required|in:1,0',                                
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(StatesDataTable $state)
    {
        //
        return $state->render('states.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Arr::only($request->all(),$this->attributes);
        $validate = Validator::make($input,$this->validationRules);
        if($validate->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validate);
        
        $input['country_id']=101;    
        if(State::create($input)){
            flash()->ok("State added successfully !");
            return redirect()->route('states.index');
        }
        flash()->e();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $State = State::withoutGlobalScope('only_active')->findOrFail($id);
        return view('states.edit',compact('State'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $State = State::withoutGlobalScope('only_active')->findOrFail($id);
        $input = Arr::only($request->all(),$this->attributes);
        $rules = $this->validationRules;
        $rules['title']="required|min:2|regex:/^[\pL\s\-]+$/u|max:300|unique:states,title,$id,id,deleted_at,NUll";
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validate);
        
        // $input['country_id']=101;    
        if($State->update($input)){
            flash()->ok("State updated successfully !");
            return redirect()->route('states.index');
        }
        flash()->e();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(State::findOrFail($id)->delete())
            return api()->success(['message'=>"Action completed successfully !"]);

        return api()->error();
    }
}

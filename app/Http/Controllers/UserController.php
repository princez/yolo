<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;

use App\DataTables\UsersDataTable;
use App\User;

class UserController extends Controller
{
    protected $attributes = [
        'role',
        'full_name',
        'profile',
        'mobile',
        'email',
        'password'
    ];

    protected $validationRules =[
        'full_name'=>'required|min:2|max:200',
        'profile'=>'nullable|image|max:8000',
        'email'=>'required|max:200|email|unique:users,email,NULL,id,deleted_at,NULL',
        'mobile'=>'required|regex:^[6789]\d{9}$^',
        // 'gender_id'=>'required|exists:genders,id',
        'role'=>"required|in:user,partner",
        'password'=>'required|min:6|max:20'
    ];   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $User)
    {
        //

        return $User->render('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
        if($req->hasFile('profile')){
            $input['profile']  = upload($req->file('profile'),'profile/');
        }

        $input['password']=bcrypt($input['password']);
        $input['is_approved']=1;
        
       if(User::create($input)){
           flash()->ok('User Created Successfully !');
           return redirect()->route('users.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $User = User::findOrFail($id);
        return view('users.edit',compact('User'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $User = User::findOrFail($id);
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;
        $rules['password']=str_replace('required','nullable',$rules['password']);
        $rules['email']="required|min:2|max:200|unique:users,email,$id,id,deleted_at,NULL";
        unset($rules['role']);
        unset($input['role']);
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());

        if($req->get('password'))
            $input['password'] = bcrypt($input['password']);
        else
            unset($input['password']);        

        if($req->hasFile('profile'))
            $input['profile']  = upload($req->file('profile'),'profile/');
        else
            $input['profile'] = $User->profile;


       if($User->update($input)){
           flash()->ok('User Updated Successfully !');
           return redirect()->route('users.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(User::findOrFail($id)->delete())
            return api()->success(['message'=>"User deleted successfully !"]);

        return api()->error();
    }

    public function approve($id){
        $User = User::whereRole('partner')
                        ->whereIsApproved(0)
                        ->find($id);
        if($User->update(['is_approved'=>1]))
            flash()->ok('Partner approved Successfully !');
        else    
            flash()->err();

            return redirect()->back();
    }

    public function ban($id,$s){
        $User = User::find($id);
        if($User->update(['is_banned'=>$s]))
            flash()->ok('Action completed Successfully !');
        else    
            flash()->err();
            
            return redirect()->back();
    }
}

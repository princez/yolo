<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;

use App\DataTables\StatesDataTable;
use App\State;
use App\Ngo;
use App\Campaign;
use App\CampaignType;
use App\Gallery;
 
class CampaignController extends Controller
{
    protected $attributes =[
        'campaign_type_id',
        'ngo_id',
        'name',
        'description',
        'no_of_meals',
        'cost_per_meal',
        'end_date',
        'thumbnail',
        
        
        
    ];

    protected $validationRules = [
        'name'=>'required|min:2|max:200',
        'campaign_type_id'=>'required|exists:campaign_types,id,deleted_at,NULL',
        'ngo_id'=>'required|exists:ngos,id,deleted_at,NULL',
        'name'=>'required|max:200',
        'description'=>'required',
        'no_of_meals'=>'required|numeric',
        'cost_per_meal'=>'required|numeric',
        'end_date'=>"required|date|after:",
        'thumbnail'=>'required|image',
                                       
    ];

    protected $friendlyNames = [
        'campaign_type_id'=>'Campaign Type',
        'ngo_id'=>'Non Profit Organization'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Campaign::orderBy('updated_at','DESC')
                    ->get();
        return view('campaigns.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $types = CampaignType::select('id','name')->get();
        $ngos = Ngo::select('id','name')->get();
        return view('campaigns.create',compact('types','ngos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Arr::only($request->all(),$this->attributes);
        $rules = $this->validationRules;
        $rules['end_date'].=Date('Y-m-d'); 
        $validate = Validator::make($input,$rules);
        $validate->setAttributeNames($this->friendlyNames);
        if($validate->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validate);
        
        $input['thumbnail']=upload($request->file('thumbnail'),'campaigns/');

        if(Campaign::create($input)){
            flash()->ok("Campaign added successfully !");
            return redirect()->route('campaigns.index');
        }
        flash()->e();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Data = Ngo::findOrFail($id);
        $states = State::orderBy('title','ASC')->select('id','title')->get();
        return view('ngos.edit',compact('Data','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Ngo = Ngo::findOrFail($id);
        $input = Arr::only($request->all(),$this->attributes);
        $rules = $this->validationRules;
        $rules['end_date'].=Date('Y-m-d');
        $rules['thumbnail']=str_replace('required','nullable',$rules['thumbnail']);
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validate);
        
        if($request->hasFile('thumbnail')){
            $input['thumbnail']=upload($request->file('thumbnail'),'campaigns/');
        }else{
            unset($input['thumbnail']);
        }
        if($Ngo->update($input)){
            flash()->ok("Campaign updated successfully !");
            return redirect()->route('campaigns.index');
        }
        flash()->e();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Campaign::findOrFail($id)->delete())
            return api()->success(['message'=>"Action completed successfully !"]);

        return api()->error();
    }

    public function gallery($id,Request $request){
        $Campaign = Campaign::findOrFail($id);
        $images = Gallery::whereCampaignId($id)
                            // ->latest('id')
                            ->get();
        return view('campaigns.gallery',compact('images','Campaign'));
    }

    public function galleryStore($id,Request $request){
        $Campaign = Campaign::findOrFail($id);

        $input =Arr::only($request->all(),['image']);
        $rules = [
            'image'=>'required|array|min:1',
            'image.*'=>'required|image'
        ];
        $input['campaign_id']=$id;
        $validator = Validator::make($input,$rules);
        if($validator->fails()){
           
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        foreach($request->file('image') as $image){
            $input['image']=upload($image,'campaign_banners/');
             
            Gallery::create($input);
        }
        
            flash()->ok("Gallery updated successfully !");
    
        
        return redirect()->back();
    }
    
    public function galleryDestroy($id){
        if(Gallery::findOrFail($id)->delete())
             return api()->success(['message'=>"Action completed successfully !"]);

        return api()->error();
        
    }
}

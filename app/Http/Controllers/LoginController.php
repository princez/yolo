<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Auth;
use \Validator;

class LoginController extends Controller
{
    //
    public function index(){
        return view('login');
    }

    public function login(Request $req){
    	
        $input = Arr::only($req->all(),[
                                                 'email',
                                                 'password'
                                             ]);
         $rules = [
                         'email'=>'required|email',
                         'password'=>'required'
         ];
         $input['role']='admin';
         $validate = Validator::make($input,$rules);
         if($validate->fails())
             return redirect()->back()->withErrors($validate)->withInput($req->all());
 
         
         if(!Auth::attempt($input)){
             flash()->e("The entered password is invalid");
             return redirect()->back()->withInput($req->all());
         }
 
         flash()->ok("Welcome ".Auth::user()->full_name,true);
 
         return redirect()->route('home');
 
     }
 
     public function logout(){
         Auth::logout();
         flash()->ok("Account Logged out successfully !");
         return redirect()->route('login');
        // return redirect()->back();
     }
}

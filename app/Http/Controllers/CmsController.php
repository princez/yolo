<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

use \Validator;

use App\DataTables\CmsDataTable;
use App\Cms;

class CmsController extends Controller
{
    protected $attributes = [
        'title',
        'content',
        'description'
    ];

    protected $validationRules =[
        'title'=>'required|min:2|max:200',
        'content'=>'required',
        
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CmsDataTable $table)
    {
        //
        return $table->render('cms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());

        $slug = Str::slug($input['title']);
        if(Cms::whereSlug($slug)->count()){
            do{
                $slug=$slug.'-'.mt_rand(1,9999);
            }while(Cms::whereSlug($slug)->count());
        }
        
        $input['slug']=$slug;
        
           
        
       if(Cms::create($input)){
           flash()->ok('Content added Successfully !');
           return redirect()->route('cms.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Cms = Cms::findOrFail($id);

        return view('cms.edit',compact('Cms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $Cms = Cms::findOrFail($id);

        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
        $Cms->delete();    
            
        $slug = Str::slug($input['title']);
       
        if(Cms::whereSlug($slug)->count()){
            do{
                $slug=$slug.'-'.mt_rand(1,9999);
            }while(Cms::whereSlug($slug)->count());
        }
        
        $input['slug']=$slug;
        
           
        
       if(Cms::create($input)){
           flash()->ok('Content updated Successfully !');
           return redirect()->route('cms.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Cms::findOrFail($id)->delete())
            return api()->success(['message'=>"Content deleted successfully !"]);

        return api()->error();
    }
}

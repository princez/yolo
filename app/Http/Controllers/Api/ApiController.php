<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;
use \JWTAuth;
use \Auth;

use App\User;
use App\Campaign;


class ApiController extends Controller
{
    //
    protected $USER;
    protected $USER_ID;
    public function __construct(){
        $this->middleware(function($request,$next){
            $this->USER=Auth::user();
            $this->USER_ID = $this->USER->id??null;
            return $next($request);
        });
    }

   

   

    public function signUp(Request $req,Otp $Otp){
        $input = Arr::only($req->all(),[
                             'full_name',
                             'email',
                             'mobile',
                             'gender_id',
                             'role',
                             'dob'               
        ]);

        $rules= [
            'full_name'=>'required|max:200|min:2',
            'email'=>'required|max:200|email|unique:users,email,NULL,id,deleted_at,NULL',
            'mobile'=>'required|regex:^[6789]\d{9}$^',
            'gender_id'=>'required|exists:genders,id',
            'role'=>"required|in:user,partner",
            'dob'=>'required|date|date_format:Y-m-d|olderThan:8'
        ];

        $errorMessages =[
            'email.unique'=>"The email account is already registered. Please Log In"
        ];

        $validator = Validator::make($input,$rules,$errorMessages);
        if($validator->fails()){
            return api()->notValid(['errorMsg'=>$validator->errors()->first()]);
        }

        $s= TempUser::create($input);
        // $s=0;
        if(!$s){
            return api()->error();
        }

        $OTP = $Otp->init($s->email,$s->id);
        if(!empty($OTP['error'])){
            return api()->notValid(['errorMsg'=>$OTP['error']]);
        }    
        
        return api()->success(['otp_id'=>$OTP->token,'message'=>'OTP sent successfully !']); 
    }

    public function tempSignIn(Request $req){
       
        $USER = User::whereRole('user')->first();
        if(!$token=JWTAuth::fromUser($USER))
    		return api()->error();


    	return api()->success(['token'=>$token,'message'=>'Logged in Successfully !']);
    }

  
    public function getStaticContent($slug){
       
        $cms = \App\Cms::select('title','content')->whereSlug($slug)->first();
        if(!$cms){
            return api()->notFound(['errorMsg'=>'Slug not found !']);
        }

        return api()->success(['data'=>$cms]);
    }

    public function getCampaigns(){
        $campaigns = Campaign::whereDate('end_date','>=',Date('Y-m-d'))
                                ->join('campaign_types','campaign_types.id','=','campaigns.campaign_type_id')
                                ->select(
                                    'campaigns.id',
                                    'campaigns.name',
                                    'campaigns.end_date',
                                    'campaigns.thumbnail',
                                    'campaign_types.name as campaign_type',
                                    \DB::raw('0 as progress'),
                                    \DB::raw("DATEDIFF(end_date,DATE(now())) as days_left")
                                )
                                ->paginate()->toArray();

        
        $campaigns['error']=false;

        return $campaigns;
    }

    public function getCampaign($id){
        $campaign = Campaign::where('campaigns.id',$id)
                    ->join('campaign_types','campaign_types.id','=','campaigns.campaign_type_id')
                    ->with('gallery')
                    ->select(
                        'campaigns.id',
                        'campaigns.name',
                        'campaigns.end_date',
                        'campaigns.thumbnail',
                        'campaign_types.name as campaign_type',
                        \DB::raw('0 as progress'),
                        \DB::raw("DATEDIFF(end_date,DATE(now())) as days_left")
                    )->first();
        
        if(!$campaign){
            return api()->notFound(['errorMsg'=>'Campaign not found !']);
        }

        return api()->success(['data'=>$campaign]);
                
    }
    

    

   
}

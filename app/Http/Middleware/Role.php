<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        $USER = \App\User::find(\Auth::user()->id);
        if($USER->role!=$role)
            return api()->custom(['errorMsg'=>'Sorry you do not have access for following action !'],401);

        if(!$USER->is_approved)
            return api()->custom(['errorMsg'=>'Your account has not been approved by admin yet !'],401);
        
        if($USER->is_banned)
            return api()->custom(['errorMsg'=>'Your account has been banned. Please contact Admin !'],401);

        $request->merge(['USER'=>$USER]);    
        
        return $next($request);
    }
}

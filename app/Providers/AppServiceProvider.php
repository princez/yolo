<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $file = app_path('Helpers/Helper.php');
        if (file_exists($file)) {
            require_once($file);
        }
       
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Validator::extend('olderThan', function($attribute, $value, $parameters)
        {
            try{
                $minAge = ( ! empty($parameters)) ? (int) $parameters[0] : 13;
                return (new \DateTime)->diff(new \DateTime($value))->y >= $minAge;
         
            }catch(\Exception $e){
                    return false;
            }
           
            // or the same using Carbon:
            // return Carbon\Carbon::now()->diff(new Carbon\Carbon($value))->y >= $minAge;
        });

        \Validator::replacer('olderThan', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':age',$parameters[0]??13,$message);
        });

        \Validator::extend('greater_than_field', function($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data = $validator->getData();
            $min_value = $data[$min_field];
            return $value >= $min_value;
          });   
      
         \Validator::replacer('greater_than_field', function($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
          });
    }
}

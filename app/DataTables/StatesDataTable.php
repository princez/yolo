<?php

namespace App\DataTables;

use App\State;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class StatesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('actions', 'states.actions')
            ->editColumn('is_active', function($o){
                if($o->is_active){
                    return "<span class='badge badge-success'>Yes</span>";
                }else{
                    return "<span class='badge badge-danger'>No</span>";
                }
                
            })
            ->editColumn('updated_at',function($obj){
                return getDateColumn($obj);
            })
            ->rawColumns(['updated_at','actions','is_active']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(State $model)
    {
        return $model->newQuery()
                        ->withoutGlobalScope('only_active')
                        ->select('states.*')
                        // ->orderBy('title','ASC')
                        ;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('sector-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                   
                    ->buttons(
                        // Button::make('create2'),
                        // Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }
    

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           
            Column::make('id'),
          
            Column::make('title')
            ->title('City'),
            Column::make('is_active')
            ,
            Column::make('updated_at')
                   ,
         
            Column::computed('actions')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'states' . date('YmdHis');
    }
}

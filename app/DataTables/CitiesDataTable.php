<?php

namespace App\DataTables;

use App\City;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CitiesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('actions', 'cities.actions')
            ->editColumn('is_active', function($o){
                if($o->is_active){
                    return "<span class='badge badge-success'>Yes</span>";
                }else{
                    return "<span class='badge badge-danger'>No</span>";
                }
                
            })
            ->editColumn('updated_at',function($obj){
                return getDateColumn($obj);
            })
            ->rawColumns(['updated_at','actions','is_active']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(City $model)
    {
        return $model->newQuery()
                        ->withoutGlobalScope('only_active')
                        ->join('states','states.id','=','state_id')
                        ->select(
                            'states.title as state',
                            'cities.*'
                            )
                       
                        ;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('sector-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                   
                    ->buttons(
                        // Button::make('create2'),
                        // Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }
    

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           
            Column::make('id'),
            Column::make('state')
            ->name('states.title'),
            // Column::make('city')
            // ->searchable(false),
            Column::make('title')
            ->title('City'),
          
            Column::make('updated_at')
                    ->orderable(false),
            Column::make('is_active')
                    ,
            Column::computed('actions')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Categories_' . date('YmdHis');
    }
}

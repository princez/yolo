<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'users.actions')
            ->addColumn('status', 'users.action_status')
            ->editColumn('updated_at',function($obj){
                return getDateColumn($obj);
            })
            ->editColumn('role',function($r){
                return ucfirst($r->role);
            })
            ->editColumn('gender',function($r){
                return ucfirst($r->gender);
            })
            ->rawColumns(['updated_at','action','status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->leftJoin('genders','genders.id','=','users.gender_id')
                    ->where('role','!=','admin')
                    ->select('users.*','genders.name as gender')
                    ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    { 
        return $this->builder()
        ->setTableId('categories-table')
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(1)
       
        ->buttons(
            // Button::make('create2'),
            // Button::make('export'),
            Button::make('print'),
            Button::make('reset'),
            Button::make('reload')
        );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           
           
                Column::make('role'),
                Column::make('full_name')
                    ->title("Name"),
                Column::make('mobile')
                    ->orderable(false),
                Column::make('email'),
                Column::make('gender'),
                Column::make('updated_at')
                        ->orderable(false),
                Column::computed('status')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
                Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
           
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}

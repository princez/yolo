<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    //

    protected $fillable =[
        'campaign_type_id',
        'ngo_id',
        'name',
        'description',
        'no_of_meals',
        'cost_per_meal',
        'end_date',
        'thumbnail'  
    ];

    protected $hidden = ['deleted_at']; 

    public function getThumbnailAttribute($v){
        return url($v);
    }

    public function gallery(){
        return $this->hasMany('App\Gallery','campaign_id');
    }


}

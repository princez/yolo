-- ALTER TABLE `users` CHANGE `role` `role` ENUM('user','admin','partener') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'user';
-- create table genders (id int AUTO_INCREMENT PRIMARY key,name varchar(100) not null ,status boolean default true);
-- CREATE TABLE `event`.`carts` ( `id` INT NOT NULL AUTO_INCREMENT , `type` ENUM('event','product') NOT NULL DEFAULT 'event' , `event_id` INT NULL , `date_id` INT NULL , `seat_id` INT NULL , `quantity` INT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
-- ALTER TABLE `carts` ADD `user_id` INT NOT NULL AFTER `id`;
-- CREATE TABLE `review_comments` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(300) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
-- INSERT INTO `review_comments` (`id`, `name`) VALUES (NULL, 'Music was bad'), (NULL, 'Food was amazing')
-- CREATE TABLE `review_comment_mappings` ( `review_id` INT NOT NULL , `comment_id` INT NOT NULL ) ENGINE = InnoDB;
-- ALTER TABLE `tickets` ADD `front_id` VARCHAR(30) NULL AFTER `id`;
-- ALTER TABLE `tickets` CHANGE `message` `message` VARCHAR(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

-- CREATE TABLE `ticket_messages` ( `id` INT NOT NULL AUTO_INCREMENT , `ticket_id` INT NOT NULL , `message` VARCHAR(500) NOT NULL , `created_at` DATETIME NOT NULL , `updated_at` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
-- ALTER TABLE `ticket_messages` ADD `user_id` INT NOT NULL AFTER `ticket_id`;

-- CREATE TABLE `event`.`order_seats` ( `order_id` INT NOT NULL , `event_id` INT NOT NULL , `event_date_id` INT NOT NULL , `seat_id` INT NOT NULL , `quantity` INT NOT NULL , `price` FLOAT NOT NULL ) ENGINE = InnoDB;
-- CREATE TABLE `event`.`order_products` ( `order_id` INT NOT NULL , `product_id` INT NOT NULL , `quantity` INT NOT NULL , `price` FLOAT NOT NULL ) ENGINE = InnoDB;
-- ALTER TABLE `orders` ADD `status` ENUM('pending','failed','completed') NOT NULL DEFAULT 'pending' AFTER `id`;
-- ALTER TABLE `orders`
--   DROP `event_id`,
--   DROP `no_of_tickets`;
--   ALTER TABLE `order_seats` ADD `subtotal` FLOAT NOT NULL AFTER `price`;
--   ALTER TABLE `order_seats` CHANGE `event_date_id` `date_id` INT(11) NOT NULL;


-- ALTER TABLE `products`  ADD `category_id` INT NULL  AFTER `id`;
-- ALTER TABLE `carts` ADD `product_id` INT NULL AFTER `type`;
-- CREATE TABLE `event`.`product_ratings` ( `id` INT NOT NULL , `user_id` INT NOT NULL , `product_id` INT NOT NULL , `rating` INT NOT NULL , `review` VARCHAR(500) NOT NULL , `created_at` DATETIME NOT NULL , `updated_at` DATETIME NOT NULL ) ENGINE = InnoDB;
-- ALTER TABLE `users` CHANGE `full_name` `full_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `mobile` `mobile` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `profile` `profile` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'ups/profile/default.jpg', CHANGE `dob` `dob` DATE NULL DEFAULT NULL, CHANGE `gender_id` `gender_id` INT(11) NULL DEFAULT NULL, CHANGE `password` `password` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
-- create table event_product_mappings(product_id int,event_id int)
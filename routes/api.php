<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('temp/sign/in','ApiController@tempSignIn');




Route::group(['middleware'=>'jwt'],function(){
    
       Route::get('get/campaigns','ApiController@getCampaigns');

       Route::get('get/campaign/{id}','ApiController@getCampaign');

});

Route::get('static/content/{slug}','ApiController@getStaticContent');



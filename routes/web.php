<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test',function(){

 
    sendMail("www.princearora@gmail.com","OTP Verification for Email","mails.text",['text'=>'Hello there ']);

});

Route::get('login',['as'=>'login','uses'=>"LoginController@index"]);

Route::post('login',['as'=>'login','uses'=>"LoginController@login"]);

Route::group(['middleware'=>'Admin'],function(){

    Route::get('logout',['as'=>'logout','uses'=>'LoginController@logout']);

    Route::get('/',['as'=>'home','uses'=>"ViewController@home"]);

    Route::resource('types','CampaignTypeController');

    Route::resource('states','StateController');

    Route::resource('cities','CityController');

    Route::resource('ngos','NgoController');

    Route::resource('users','UserController');

  
   
    Route::resource('cms','CmsController');

    Route::resource('campaigns','CampaignController');

    Route::get('campaigns/gallery/{campaign_id}',['as'=>'campaigns.gallery','uses'=>'CampaignController@gallery']);

    Route::post('campaigns/gallery/{campaign_id}',['as'=>'campaigns.gallery','uses'=>'CampaignController@galleryStore']);

    Route::delete('campaigns/gallery/{image_id}',['as'=>'campaigns.gallery.destroy','uses'=>'CampaignController@galleryDestroy']);
    
    

    Route::group(['prefix'=>'ajax','as'=>'ajax.'],function(){

        Route::post('upload/media',['as'=>'uploadMedia','uses'=>'AjaxController@uploadMedia']);

        Route::any('get/cities/{state_id?}',['as'=>'getCities','uses'=>'AjaxController@getCities']);

    });

});

/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	// config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'ckeditor_wiris';
	config.allowedContent = true;
	config.removePlugins = 'easyimage';
	// config.extraPlugins = ',image2';
    // config.extraPlugins +=",N1ED-editor";

// 	config.toolbarGroups = [
// 	'ckeditor_wiris_formulaEditor', 
//     { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
//     { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
//     { name: 'links' },
//     { name: 'insert' },
//     { name: 'forms' },
//     { name: 'tools' },
//     { name: 'document',       groups: [ 'mode', 'document', 'doctools' ] },
//     { name: 'others' },
//     '/',
//     { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
//     { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
//     { name: 'styles' },
//     { name: 'colors' },
//     { name: 'about' }
// ];
};
 (function () {
            var _createObjectURL = window.URL.createObjectURL;
            Object.defineProperty(window.URL, 'createObjectURL', {
                set: function (value) {
                    console.trace('set createObjectURL')
                    _createObjectURL = value;
                },
                get: function () {
                    console.trace('get createObjectURL')
                    return _createObjectURL;
                }
            })
        })();
        (function () {
            var _URL = window.URL;
            Object.defineProperty(window, 'URL', {
                set: function (value) {
                    console.trace('set URL')
                    _URL = value;
                },
                get: function () {
                    console.trace('get URL')
                    return _URL;
                }
            })
        })();  
